class FroalaAttachmentsAdapter {
    constructor(resource, field) {
        this.resource = resource;
        this.field = field;
    }

    get cleanUpUrl() {
        console.log(this.field.attribute);

        let fAtt= this.field.attribute;

        let placeOfChar =  fAtt.includes('[')?fAtt.indexOf("["): fAtt.indexOf("_");

         console.log('>>>' + fAtt.substr(0, placeOfChar));
        let correctFieldName= fAtt.substr(0, placeOfChar);


        console.log('>>>' + correctFieldName);


        if (fAtt.includes('[') || fAtt.includes('_') ) {
            return `/nova-vendor/froala-field/${this.resource}/attachments/${correctFieldName}/${
                this.field.draftId
            }`;
         }

         return `/nova-vendor/froala-field/${this.resource}/attachments/${this.field.attribute}/${
            this.field.draftId
          }`;


    }

    get imageUploadUrl() {
       // return this.field.attribute
        //return `/nova-vendor/froala-field/${this.resource}/attachments/${this.field.attribute}`;
//        console.log(this.field.attribute);
        let fAtt= this.field.attribute;
        let placeOfChar =  fAtt.includes('[')?fAtt.indexOf("["): fAtt.indexOf("_");

        //let placeOfChar = fAtt.indexOf("[");

        // console.log(fAtt.substr(0, placeOfChar));
        let correctFieldName= fAtt.substr(0, placeOfChar);
        if (fAtt.includes('[') || fAtt.includes('_') ) {
           return `/nova-vendor/froala-field/${this.resource}/attachments/${correctFieldName}`;
        }
           return `/nova-vendor/froala-field/${this.resource}/attachments/${this.field.attribute}`;

        }


    get imageRemoveUrl() {
        return this.imageUploadUrl;
    }

    get vieoUploadUrl() {
        return this.imageUploadUrl;
    }

    get fileUploadUrl() {
        return this.imageUploadUrl;
    }
}

export default FroalaAttachmentsAdapter;
